package s05;

public class Ex1 {
    public static void main(String[] args) {
        int e = 4, f = 5;
        int c = Math.abs(e - f) % 2;
                            // |e-f| % 2 == c
        if (e > 0) {
            e = e - 1;      // |(e+1)-f| % 2 == c
            f = f + 1;      // |(e+1)-(f-1)| % 2 == c
        } else {
            e = e + 1;      // |(e-1)-f| % 2 == c
            f = f - 1;      // |(e-1)-(f+1)| % 2 == c
        }
                            // |e-f| % 2 == c
    }
    /*
    b)
    e*f : oui
    e-f : oui
    e^f : non (la puissance changera le calcul)
    e+f : oui
     */
}
