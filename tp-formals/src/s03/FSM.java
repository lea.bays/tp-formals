package s03;

import java.util.HashSet;
import java.util.Set;

public class FSM {
    private int[][] transition;  // copy of the transition table
    private Set<Integer> acceptingStates;

    public FSM(int[][] transitions,        // also gives n and m
               Set<Integer> acceptingStates) {
        int nStates = transitions.length;
        int nSymbols = transitions[0].length;
        this.transition = transitions;
        this.acceptingStates = acceptingStates;
    }

    public boolean accepts(int[] word) {
        int crtState = 0;
        for (int s : word) {
            crtState = transition[crtState][s];
        }
        return acceptingStates.contains(crtState);
    }

    public int nStates() {
        return transition.length;
    }

    public int nSymbols() {
        return transition[0].length;
    }

    // -----------------------------------------------------------
    public static void main(String[] args) {
        if (args.length == 0)
            args = new String[]{"abbab", "baab", "bbabbb"};

        /*      a   b
            0   1   0
            1   2   0
            2   2   2
         */
        int[][] transitions = {
                {1, 0},         // <-- from state 0, with symbol a,b
                {2, 0},         // <-- from state 1, with symbol a,b
                {2, 2},         // <-- from state 2, with symbol a,b
        };
        Set<Integer> acceptingStates = new HashSet<>();
        acceptingStates.add(0);
        FSM autom = new FSM(transitions, acceptingStates);
        for (int i = 0; i < args.length; i++) {
            String letters = args[i];
            int[] word = new int[letters.length()];
            for (int j = 0; j < letters.length(); j++)
                word[j] = letters.charAt(j) - 'a';
            boolean res = autom.accepts(word);
            System.out.println(letters + " : " + res);
        }
    }
}