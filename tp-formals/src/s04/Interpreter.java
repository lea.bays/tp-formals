package s04;

import java.util.HashMap;

public class Interpreter {
    private static Lexer lexer;
    // TODO - A COMPLETER (ex. 3)

    public static int evaluate(String e) throws ExprException {
        lexer = new Lexer(e);
        int res = parseExpr();
        // test if nothing follows the expression...
        if (lexer.crtSymbol().length() > 0)
            throw new ExprException("bad suffix");
        return res;
    }

    private static int parseExpr() throws ExprException {
        int res = parseTerm();
        while (true) {
            if (lexer.isMinus()) {
                lexer.goToNextSymbol();
                res -= parseTerm();
            } else if (lexer.isPlus()) {
                lexer.goToNextSymbol();
                res += parseTerm();
            } else {
                break;
            }
        }
        return res;
    }

    private static int parseTerm() throws ExprException {
        int res = parseFact();
        while (true) {
            if (lexer.isSlash()) {
                lexer.goToNextSymbol();
                res /= parseFact();
            } else if (lexer.isStar()) {
                lexer.goToNextSymbol();
                res *= parseFact();
            } else {
                break;
            }
        }
        return res;
    }

    private static int parseFact() throws ExprException {
        int res = 0;
        if (lexer.isOpeningParenth()) {
            lexer.goToNextSymbol();
            res = parseExpr();
            if (!lexer.isClosingParenth()) throw new ExprException("Need a closing parenth !");
            lexer.goToNextSymbol();
        } else if (lexer.isNumber()) {
            res = lexer.intFromSymbol();
            lexer.goToNextSymbol();
        } else if (lexer.isIdent()) {
            String function = lexer.crtSymbol();
            lexer.goToNextSymbol();
            if (lexer.isOpeningParenth()) {
                lexer.goToNextSymbol();
                int arg = parseExpr();
                res = applyFct(function, arg);
                lexer.goToNextSymbol();
            } else return applyFct(function, 0);
        }
        return res;
    }

    private static int applyFct(String fctName, int arg) throws ExprException {
        return 0; // TODO - A COMPLETER
    }
}
