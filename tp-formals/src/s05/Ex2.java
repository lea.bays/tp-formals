package s05;

public class Ex2 {

    static int power(int b, int p) {// PRE: p > 0
        int res = 1, n = p;
        int x = b;                  // x^n * res == bp
        while (n > 0) {             // ...
            if (n % 2 == 0) {       // x^n * res == bp && n % 2 == 0
                x = x * x;          // (x*x)^n * res == bp && n % 2 == 0
                n = n / 2;          // (x*x)^(n*2) * res == bp && (n*2) % 2 == 0
            } else {                // x^n * res == bp && n % 2 != 0
                res = res * x;      // x^n * (res/x) == bp && n % 2 != 0
                n--;                // x^(n+1) * (res/x) == bp && n % 2 != 0
            }
        }
        return res;                 // x^n * res == bp
    }
}
